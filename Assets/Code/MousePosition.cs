using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePosition : MonoBehaviour
{
    public GameObject target;
    public GameObject selection_circle;
    public LayerMask layer_ground, layer_enemy;
    Camera cam;

    public bool visual_accept = false;

    public GameObject target_outline;

    public GameObject ignore_object;

    private void Start()
    {
        cam = Camera.main;
    }

    private void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, float.MaxValue, layer_ground))
        {
            transform.position = new Vector3(hit.point.x,
                                             0.65f,
                                             hit.point.z);
        }

        if (visual_accept)
        {
            selection_circle.SetActive(true);

            Ray ray1 = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit1;

            if (Physics.Raycast(ray1, out hit1, float.MaxValue, layer_enemy))
            {
                if (hit1.collider.gameObject != ignore_object)
                {
                    target = hit1.collider.gameObject;

                    target_outline.GetComponent<MeshRenderer>().enabled = true;
                    target_outline.transform.position = target.transform.position;
                }
            }
            else
            {
                target = null;
                target_outline.GetComponent<MeshRenderer>().enabled = false;
            }
        }
        else
        {
            selection_circle.SetActive(false);
            target_outline.GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
