using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WinPopUp : MonoBehaviour
{
    public TMP_Text t_hit_1, t_dmg_1;
    public TMP_Text t_hit_2, t_dmg_2;
    public TMP_Text t_hit_3, t_dmg_3;
    public TMP_Text t_hit_4, t_dmg_4;

    public List<Sprite> spr_character;
    public List<Image> img_character;

    public void Start()
    {
        t_dmg_1.text = "�������� ����� - " + PlayerPrefs.GetFloat("character" + 0 + "dmg");
        t_hit_1.text = "�������� ����� - " + PlayerPrefs.GetFloat("character" + 0 + "hit");

        t_dmg_2.text = "�������� ����� - " + PlayerPrefs.GetFloat("character" + 1 + "dmg");
        t_hit_2.text = "�������� ����� - " + PlayerPrefs.GetFloat("character" + 1 + "hit");

        t_dmg_3.text = "�������� ����� - " + PlayerPrefs.GetFloat("character" + 2 + "dmg");
        t_hit_3.text = "�������� ����� - " + PlayerPrefs.GetFloat("character" + 2 + "hit");

        t_dmg_4.text = "�������� ����� - " + PlayerPrefs.GetFloat("character" + 3 + "dmg");
        t_hit_4.text = "�������� ����� - " + PlayerPrefs.GetFloat("character" + 3 + "hit");

        img_character[0].sprite = spr_character[PlayerPrefs.GetInt("character" + 0 + "num")];
        img_character[1].sprite = spr_character[PlayerPrefs.GetInt("character" + 1 + "num")];
        img_character[2].sprite = spr_character[PlayerPrefs.GetInt("character" + 2 + "num")];
        img_character[3].sprite = spr_character[PlayerPrefs.GetInt("character" + 3 + "num")];
    }

    public void But_Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void But_Exit()
    {
        Application.LoadLevel("Menu");
    }
}
