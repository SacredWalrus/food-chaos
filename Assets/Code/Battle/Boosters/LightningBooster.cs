using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningBooster : MonoBehaviour
{
    public GameObject target;
    public LayerMask layer_ground;
    Camera cam;
    public ParticleSystem vfx;
    public SphereCollider col;

    public bool visual_accept;

    public GameObject selection_circle;

    public bool attack = false;

    public float damage;


    private void Start()
    {
        cam = Camera.main;

        visual_accept = true;
        col.enabled = false;
    }

    private void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, float.MaxValue, layer_ground))
        {
            transform.position = new Vector3(hit.point.x,
                                             0.65f,
                                             hit.point.z);
        }

        if (visual_accept)
        {
            selection_circle.SetActive(true);
        }
        else
        {
            selection_circle.SetActive(false);
        }

        if (attack)
        {
            visual_accept = false;
            vfx.Play();
            col.enabled = true;

            Destroy(gameObject, 0.5f);
            attack = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "enemy")
        {
            if (other.gameObject.GetComponent<Brain>().team != Brain.Team.team_1)
            {
                other.gameObject.GetComponent<Brain>().Hit(other.gameObject.GetComponent<Brain>().target.gameObject, damage);
            }
        }
    }
}
