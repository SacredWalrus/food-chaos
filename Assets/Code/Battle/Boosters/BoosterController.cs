using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BoosterController : MonoBehaviour
{
    //Lightning
    public int lightning_booster_count_max;
    public int lightning_booster_count;
    public TMP_Text t_lightning_booster_count;
    public Button but_lightning_booster;

    public GameObject lightning_obj;
    private GameObject inst_lightning_obj;    

    //SpeedUp
    public Button but_speed_up;
    public int speed_up_booster_count_max;
    public int speed_up_booster_count;
    public TMP_Text t_speed_up_booster_count;
    private bool speed_up_active;
    private List<GameObject> speed_up_character = new List<GameObject>();
    private List<float> speed_up_base_count = new List<float>();


    private void Start()
    {
        lightning_booster_count = lightning_booster_count_max;
        speed_up_booster_count = speed_up_booster_count_max;
    }

    private void Update()
    {
        t_lightning_booster_count.text = lightning_booster_count + "/" + lightning_booster_count_max;
        t_speed_up_booster_count.text = speed_up_booster_count + "/" + speed_up_booster_count_max;

        if (lightning_booster_count <= 0)
        {
            but_lightning_booster.interactable = false;
        } else but_lightning_booster.interactable = true;

        if (speed_up_booster_count <= 0)
        {
            but_speed_up.interactable = false;
        }
        else if (!speed_up_active) but_speed_up.interactable = true;
    }

    #region Lightning
    public void ButBoosterLightningDown()
    {
        if (lightning_booster_count > 0)
        {
            inst_lightning_obj = Instantiate(lightning_obj, transform.position, transform.rotation);
        }
    }

    public void ButBoosterLightningUp()
    {
        if (lightning_booster_count > 0)
        {
            inst_lightning_obj.GetComponent<LightningBooster>().attack = true;
            lightning_booster_count--;
        }
    }
    #endregion

    #region SpeedUp
    public void But_SpeedUp()
    {
        if (speed_up_booster_count > 0)
        {
            speed_up_active = true;
            but_speed_up.interactable = false;
            StartCoroutine(SpeedUpEnum());
            speed_up_booster_count--;
        }
    }

    IEnumerator SpeedUpEnum()
    {
        speed_up_character.Clear();
        speed_up_base_count.Clear();

        foreach (GameObject gm in GameObject.Find("GameController").GetComponent<BattleController>().active_character)
        {
            if (gm.GetComponent<Brain>().team == Brain.Team.team_1)
            {
                speed_up_character.Add(gm);
                speed_up_base_count.Add(gm.GetComponent<Brain>().speed);
                gm.GetComponent<Brain>().speed *= 2;
                gm.GetComponent<Brain>().ai_path.maxSpeed = gm.GetComponent<Brain>().speed;
            }
        }

        yield return new WaitForSeconds(3);

        for (int i = 0; i < 2; i++)
        {
            speed_up_character[i].GetComponent<Brain>().speed = speed_up_base_count[i];
            speed_up_character[i].GetComponent<Brain>().ai_path.maxSpeed = speed_up_character[i].GetComponent<Brain>().speed;
        }

        speed_up_active = false;
    }
    #endregion
}
