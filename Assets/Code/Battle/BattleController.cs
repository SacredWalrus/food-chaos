using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleController : MonoBehaviour
{
    [Header("Global")]
    public List<GameObject> active_character;

    public TMP_Text t_timer, t_team_1_kills, t_team_2_kills;
    public int round_time;
    float timer;
    public int team_1_kills, team_2_kills;

    public GameObject WinPopUp;

    [Header("Skills")]
    public List<Sprite> spr_character;
    public List<Image> img_skill_fill;
    public List<Image> img_skill_back;

    public List<float> skill_time;
    public List<bool> skill_load;

    public float skill_timer;

    public GameObject skill_1_but, skill_2_but, skill_3_but, skill_4_but;

    [Header("Spawn")]
    public List<GameObject> characters_obj;
    public List<Transform> pos_spawn;


    private void Awake()
    {
        SpawnCharacter();

        if (PlayerPrefs.GetString("play_mode") == "pve")
        {
            skill_3_but.SetActive(false);
            skill_4_but.SetActive(false);
        }

        for (int i = 0; i < 4; i++)
        {
            skill_load[i] = false;
            skill_time[i] = 0;
        }

        Application.targetFrameRate = 60;
        Time.timeScale = 1;

        timer = round_time;
    }

    private void Update()
    {
        timer -= Time.deltaTime;

        t_timer.text = (int)timer + "";
        t_team_1_kills.text = "" + team_1_kills;
        t_team_2_kills.text = "" + team_2_kills;

        if (timer <= 0)
        {
            WinPopUp.SetActive(true);
            Time.timeScale = 0;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Application.LoadLevel(Application.loadedLevel);
        }

        SkillController();
    }

    public void But_Skill(int num)
    {
        if (active_character[num].GetComponent<Brain>().die != true)
        {
            skill_load[num] = false;
            skill_time[num] = 0;

            active_character[num].GetComponent<Brain>().ulta_activate = true;
        }
    }

    void SkillController()
    {
        //1
        if (!skill_load[0])
        {
            skill_1_but.GetComponent<Button>().interactable = false;
            skill_time[0] += Time.deltaTime;

            img_skill_fill[0].fillAmount = skill_time[0] / skill_timer;

            if (skill_time[0] >= skill_timer)
            {
                skill_load[0] = true;
                skill_time[0] = skill_timer;
            }
        }

        if (skill_load[0])
        {
            skill_1_but.GetComponent<Button>().interactable = true;
        }

        //2
        if (!skill_load[1])
        {
            skill_2_but.GetComponent<Button>().interactable = false;
            skill_time[1] += Time.deltaTime;

            img_skill_fill[1].fillAmount = skill_time[1] / skill_timer;

            if (skill_time[1] >= skill_timer)
            {
                skill_load[1] = true;
                skill_time[1] = skill_timer;
            }
        }

        if (skill_load[1])
        {
            skill_2_but.GetComponent<Button>().interactable = true;
        }

        //3
        if (!skill_load[2])
        {
            skill_3_but.GetComponent<Button>().interactable = false;
            skill_time[2] += Time.deltaTime;

            img_skill_fill[2].fillAmount = skill_time[2] / skill_timer;

            if (skill_time[2] >= skill_timer)
            {
                skill_load[2] = true;
                skill_time[2] = skill_timer;
            }
        }

        if (skill_load[2])
        {
            skill_3_but.GetComponent<Button>().interactable = true;
        }

        //4
        if (!skill_load[3])
        {
            skill_4_but.GetComponent<Button>().interactable = false;
            skill_time[3] += Time.deltaTime;

            img_skill_fill[3].fillAmount = skill_time[3] / skill_timer;

            if (skill_time[3] >= skill_timer)
            {
                skill_load[3] = true;
                skill_time[3] = skill_timer;
            }
        }

        if (skill_load[3])
        {
            skill_4_but.GetComponent<Button>().interactable = true;
        }

        if (PlayerPrefs.GetString("play_mode") == "pve")
        {
            if (skill_load[2] && active_character[2].GetComponent<Brain>().die != true)
            {
                StartCoroutine(AutoSkill(2, Random.Range(1f, 7f)));
                skill_load[2] = false;
                skill_time[2] = 0;
            }

            if (skill_load[3] && active_character[3].GetComponent<Brain>().die != true)
            {
                StartCoroutine(AutoSkill(3, Random.Range(1f, 7f)));
                skill_load[3] = false;
                skill_time[3] = 0;
            }
        }
    }

    IEnumerator AutoSkill(int skill_num, float time)
    {
        yield return new WaitForSeconds(time);
        But_Skill(skill_num);
    }

    void SpawnCharacter()
    {
        List<int> character_spawn_num = new List<int>();
        for (int i = 0; i < 4; i++)
        {
            character_spawn_num.Add(PlayerPrefs.GetInt("character" + i + "num"));            
        }

        //1
        GameObject char_1 = Instantiate(characters_obj[PlayerPrefs.GetInt("character" + 0 + "num")], pos_spawn[0].position, transform.rotation);
        active_character.Add(char_1);
        char_1.GetComponent<Brain>().team = Brain.Team.team_1;
        char_1.GetComponent<Brain>().character_num = 0;

        img_skill_fill[0].sprite = spr_character[PlayerPrefs.GetInt("character" + 0 + "num")];
        img_skill_back[0].sprite = spr_character[PlayerPrefs.GetInt("character" + 0 + "num")];

        string prefs_health;
        string prefs_speed;
        string prefs_damage;

        switch (character_spawn_num[0])
        {
            case 0:
                prefs_health = "character" + 0 + "health";
                prefs_speed = "character" + 0 + "speed";
                prefs_damage = "character" + 0 + "damage";
                char_1.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_1[PlayerPrefs.GetInt(prefs_health) - 1];
                char_1.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_1[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_1.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_1[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 1:
                prefs_health = "character" + 1 + "health";
                prefs_speed = "character" + 1 + "speed";
                prefs_damage = "character" + 1 + "damage";
                char_1.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_2[PlayerPrefs.GetInt(prefs_health) - 1];
                char_1.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_2[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_1.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_2[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 2:
                prefs_health = "character" + 2 + "health";
                prefs_speed = "character" + 2 + "speed";
                prefs_damage = "character" + 2 + "damage";
                char_1.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_3[PlayerPrefs.GetInt(prefs_health) - 1];
                char_1.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_3[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_1.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_3[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 3:
                prefs_health = "character" + 3 + "health";
                prefs_speed = "character" + 3 + "speed";
                prefs_damage = "character" + 3 + "damage";
                char_1.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_4[PlayerPrefs.GetInt(prefs_health) - 1];
                char_1.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_4[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_1.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_4[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
        }      

        //2
        GameObject char_2 = Instantiate(characters_obj[PlayerPrefs.GetInt("character" + 1 + "num")], pos_spawn[1].position, transform.rotation);
        active_character.Add(char_2);
        char_2.GetComponent<Brain>().team = Brain.Team.team_1;
        char_2.GetComponent<Brain>().character_num = 1;

        img_skill_fill[1].sprite = spr_character[PlayerPrefs.GetInt("character" + 1 + "num")];
        img_skill_back[1].sprite = spr_character[PlayerPrefs.GetInt("character" + 1 + "num")];

        switch (character_spawn_num[1])
        {
            case 0:
                prefs_health = "character" + 0 + "health";
                prefs_speed = "character" + 0 + "speed";
                prefs_damage = "character" + 0 + "damage";
                char_2.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_1[PlayerPrefs.GetInt(prefs_health) - 1];
                char_2.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_1[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_2.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_1[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 1:
                prefs_health = "character" + 1 + "health";
                prefs_speed = "character" + 1 + "speed";
                prefs_damage = "character" + 1 + "damage";
                char_2.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_2[PlayerPrefs.GetInt(prefs_health) - 1];
                char_2.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_2[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_2.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_2[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 2:
                prefs_health = "character" + 2 + "health";
                prefs_speed = "character" + 2 + "speed";
                prefs_damage = "character" + 2 + "damage";
                char_2.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_3[PlayerPrefs.GetInt(prefs_health) - 1];
                char_2.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_3[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_2.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_3[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 3:
                prefs_health = "character" + 3 + "health";
                prefs_speed = "character" + 3 + "speed";
                prefs_damage = "character" + 3 + "damage";
                char_2.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_4[PlayerPrefs.GetInt(prefs_health) - 1];
                char_2.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_4[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_2.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_4[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
        }

        //3
        GameObject char_3 = Instantiate(characters_obj[PlayerPrefs.GetInt("character" + 2 + "num")], pos_spawn[2].position, transform.rotation);
        active_character.Add(char_3);
        char_3.GetComponent<Brain>().team = Brain.Team.team_2;
        char_3.GetComponent<Brain>().character_num = 2;

        img_skill_fill[2].sprite = spr_character[PlayerPrefs.GetInt("character" + 2 + "num")];
        img_skill_back[2].sprite = spr_character[PlayerPrefs.GetInt("character" + 2 + "num")];

        switch (character_spawn_num[2])
        {
            case 0:
                prefs_health = "character" + 0 + "health";
                prefs_speed = "character" + 0 + "speed";
                prefs_damage = "character" + 0 + "damage";
                char_3.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_1[PlayerPrefs.GetInt(prefs_health) - 1];
                char_3.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_1[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_3.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_1[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 1:
                prefs_health = "character" + 1 + "health";
                prefs_speed = "character" + 1 + "speed";
                prefs_damage = "character" + 1 + "damage";
                char_3.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_2[PlayerPrefs.GetInt(prefs_health) - 1];
                char_3.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_2[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_3.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_2[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 2:
                prefs_health = "character" + 2 + "health";
                prefs_speed = "character" + 2 + "speed";
                prefs_damage = "character" + 2 + "damage";
                char_3.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_3[PlayerPrefs.GetInt(prefs_health) - 1];
                char_3.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_3[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_3.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_3[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 3:
                prefs_health = "character" + 3 + "health";
                prefs_speed = "character" + 3 + "speed";
                prefs_damage = "character" + 3 + "damage";
                char_3.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_4[PlayerPrefs.GetInt(prefs_health) - 1];
                char_3.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_4[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_3.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_4[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
        }

        //4
        GameObject char_4 = Instantiate(characters_obj[PlayerPrefs.GetInt("character" + 3 + "num")], pos_spawn[3].position, transform.rotation);
        active_character.Add(char_4);
        char_4.GetComponent<Brain>().team = Brain.Team.team_2;
        char_4.GetComponent<Brain>().character_num = 3;

        img_skill_fill[3].sprite = spr_character[PlayerPrefs.GetInt("character" + 3 + "num")];
        img_skill_back[3].sprite = spr_character[PlayerPrefs.GetInt("character" + 3 + "num")];

        switch (character_spawn_num[3])
        {
            case 0:
                prefs_health = "character" + 0 + "health";
                prefs_speed = "character" + 0 + "speed";
                prefs_damage = "character" + 0 + "damage";
                char_4.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_1[PlayerPrefs.GetInt(prefs_health) - 1];
                char_4.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_1[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_4.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_1[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 1:
                prefs_health = "character" + 1 + "health";
                prefs_speed = "character" + 1 + "speed";
                prefs_damage = "character" + 1 + "damage";
                char_4.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_2[PlayerPrefs.GetInt(prefs_health) - 1];
                char_4.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_2[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_4.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_2[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 2:
                prefs_health = "character" + 2 + "health";
                prefs_speed = "character" + 2 + "speed";
                prefs_damage = "character" + 2 + "damage";
                char_4.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_3[PlayerPrefs.GetInt(prefs_health) - 1];
                char_4.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_3[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_4.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_3[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
            case 3:
                prefs_health = "character" + 3 + "health";
                prefs_speed = "character" + 3 + "speed";
                prefs_damage = "character" + 3 + "damage";
                char_4.GetComponent<Brain>().start_health = GameObject.Find("GameController").GetComponent<CharacterStats>().hp_char_4[PlayerPrefs.GetInt(prefs_health) - 1];
                char_4.GetComponent<Brain>().damage = GameObject.Find("GameController").GetComponent<CharacterStats>().dmg_char_4[PlayerPrefs.GetInt(prefs_damage) - 1];
                char_4.GetComponent<Brain>().speed = GameObject.Find("GameController").GetComponent<CharacterStats>().speed_char_4[PlayerPrefs.GetInt(prefs_speed) - 1];
                break;
        }

        PlayerPrefs.SetFloat("character" + 0 + "hit", 0);
        PlayerPrefs.SetFloat("character" + 0 + "dmg", 0);

        PlayerPrefs.SetFloat("character" + 1 + "hit", 0);
        PlayerPrefs.SetFloat("character" + 1 + "dmg", 0);

        PlayerPrefs.SetFloat("character" + 2 + "hit", 0);
        PlayerPrefs.SetFloat("character" + 2 + "dmg", 0);

        PlayerPrefs.SetFloat("character" + 3 + "hit", 0);
        PlayerPrefs.SetFloat("character" + 3 + "dmg", 0);
    }

    public void But_Back()
    {
        Application.LoadLevel("Menu");
    }
}
