using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brain : MonoBehaviour
{
    [Header("��������������")]
    public float speed;
    public float damage;
    public float health;

    [HideInInspector]
    public Pathfinding.AIDestinationSetter ai_destination;

    [HideInInspector]
    public Pathfinding.AIPath ai_path;

    CharacterController char_controller;

    public enum Team
    {
        team_1,
        team_2
    }
    public Team team;

    public bool ulta_activate;
    public bool stun = false;
    public bool shield = false;

    public Animator anim;

    public Transform target;
    public Transform target_back;

    [HideInInspector]
    public int character_num;

    [Header("���������")]
    public SkinnedMeshRenderer body;
    public SkinnedMeshRenderer head;
    public Material hit_mat_1, hit_mat_2;
    public Material head_mat, body_mat;

    public bool selection_active;
    public bool die;

    [Header("��������� ��������������")]
    public float start_health;
    public Vector3 start_pos;

    [Header("UI")]
    public GameObject canvas_ui;
    public Image hpbar_fill;
    public Image hpbar_back;
    public Color color_team_1;
    public Color color_team_2;
    public Color color_back_team_1;
    public Color color_back_team_2;

    [Header("Selection Circle")]
    public MeshRenderer selection_circle_rend;
    public Material mat_team_1;
    public Material mat_team_2;


    private void Awake()
    {
        ai_destination = GetComponent<Pathfinding.AIDestinationSetter>();
        ai_path = GetComponent<Pathfinding.AIPath>();
        char_controller = GetComponent<CharacterController>();
    }

    public void Start()
    {
        if (team == Team.team_1)
        {
            hpbar_fill.color = color_team_1;
            hpbar_back.color = color_back_team_1;
            selection_circle_rend.material = mat_team_1;
        }
        else
        {
            hpbar_fill.color = color_team_2;
            hpbar_back.color = color_back_team_2;
            selection_circle_rend.material = mat_team_2;
        }

        ai_path.maxSpeed = speed;

        start_pos = transform.position;
        health = start_health;
    }

    private void Update()
    {
        anim.SetFloat("speed", char_controller.velocity.magnitude);
        UICanvas();

        if (die) canvas_ui.SetActive(false);
        else canvas_ui.SetActive(true);


        if (target == null && !die)
        {
            FindTarget();
        }

        if (target != null && target.GetComponent<Brain>())
        {
            if (target.GetComponent<Brain>().die)
            {
                target = null;
                selection_active = false;
            }
        }
    }

    void UICanvas()
    {
        canvas_ui.transform.LookAt(Camera.main.transform);
        hpbar_fill.fillAmount = health / start_health;
    }

    public void Movement(Transform target_pos)
    {
        ai_destination.target = target_pos;
        ai_path.canMove = true;
    }

    public void StopMovement()
    {
        ai_path.canMove = false;
        char_controller.Move(new Vector3(0, 0, 0));
    }

    public void MoveBack()
    {       
        bool isDirSafe = false;

        //We will need to rotate the direction away from the player if straight to the opposite of the player is a wall
        float vRotation = 0;


        while (!isDirSafe)
        {
            Vector3 dir_to_enemy = transform.position - target.position;
            Vector3 newPos = transform.position + dir_to_enemy;

            //Rotate the direction of the Enemy to move
            newPos = Quaternion.Euler(0, vRotation, 0) * newPos;

            target_back.position = newPos;

            //Shoot a Raycast out to the new direction with 5f length (as example raycast length) and see if it hits an obstacle
            bool isHit = Physics.Raycast(transform.position, newPos, out RaycastHit hit, 3f);

            if (hit.transform == null)
            {
                //If the Raycast to the flee direction doesn't hit a wall then the Enemy is good to go to this direction
                MovementBack(target_back);
                //char_controller.Move(transform.forward * speed * Time.deltaTime);
                isDirSafe = true;
            }

            //Change the direction of fleeing is it hits a wall by 20 degrees
            if (isHit && hit.transform.CompareTag("obstacles"))
            {
                vRotation += 20;
                isDirSafe = false;
            }
            else
            {
                //If the Raycast to the flee direction doesn't hit a wall then the Enemy is good to go to this direction
                //char_controller.Move(transform.forward * speed * Time.deltaTime);
                MovementBack(target_back);
                isDirSafe = true;
            }
        }
    }

    public void MovementBack(Transform target_pos)
    {
        ai_destination.target = target_pos;
        ai_path.canMove = true;
    }

    public void FindTarget()
    {
        GameObject tg = null;
        int counter = 0;

        while (tg == null)
        {
            counter++;

            List<GameObject> enemy = new List<GameObject>();

            foreach (GameObject gm in GameObject.Find("GameController").GetComponent<BattleController>().active_character)
            {
                enemy.Add(gm);
            }

            float min_dist = 9999;
            int num = -1;

            for (int i = 0; i < enemy.Count; i++)
            {
                if (enemy[i].GetComponent<Brain>().team != team && !enemy[i].GetComponent<Brain>().die)
                {
                    if (Vector3.Distance(transform.position, enemy[i].transform.position) < min_dist)
                    {
                        num = i;
                        min_dist = Vector3.Distance(transform.position, enemy[i].transform.position);
                    }
                }                
            }

            if (num >= 0)
            {
                target = enemy[num].transform;
            } else
            {
                target = null;
            }

            if (counter >= 10) return;
        }
    }

    public Vector3 GenerateRandomPoint()
    {
        Vector3 target_pos = transform.forward - transform.forward * Random.Range(1f, 4f);
        target_pos += transform.right * Random.Range(-4f, 4f);

        return target_pos;
    }

    #region Hit
    public void Hit(GameObject new_target, float hit_damage)
    {       
        if (!die && !shield)
        {
            StartCoroutine(HitEnum());

            health -= hit_damage;

            if (!selection_active)
            {
                if (Vector3.Distance(transform.position, new_target.transform.position) < 3 && !selection_active)
                    target = new_target.transform;
            }

            PlayerPrefs.SetFloat("character" + character_num + "hit", PlayerPrefs.GetFloat("character" + character_num + "hit") + hit_damage);
            PlayerPrefs.SetFloat("character" + new_target.GetComponent<Brain>().character_num + "dmg", PlayerPrefs.GetFloat("character" + new_target.GetComponent<Brain>().character_num + "dmg") + hit_damage);
        }
    }

    public IEnumerator HitEnum()
    {
        //On 1
        body.material = hit_mat_1;
        head.material = hit_mat_1;

        yield return new WaitForSeconds(0.05f);

        //Off
        body.material = body_mat;
        head.material = head_mat;

        yield return new WaitForSeconds(0.05f);

        //On 2
        body.material = hit_mat_2;
        head.material = hit_mat_2;

        yield return new WaitForSeconds(0.05f);

        body.material = body_mat;
        head.material = head_mat;
    }    
    #endregion
}
