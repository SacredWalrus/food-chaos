using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassTank : MonoBehaviour
{
    public enum State
    {
        none,
        RoundStart,
        MoveToTarget,
        Die,
        Attack,
        Ulta,
        MoveToSelection
    }
    public State curr_state;

    Brain brain;    

    public float rotation_speed;

    public float min_dist_attack;  //����������� ��������� ��� �����
    public float attack_cd;  //��� ���� �����
    public float attack_time;

    [HideInInspector]
    public bool attack_start;
    [HideInInspector]
    public bool attack_access;

    private void Start()
    {
        brain = GetComponent<Brain>();

        attack_access = true;        
    }

    private void Update()
    {
        if (brain.ulta_activate && !brain.stun)
        {
            StopAction();
            curr_state = State.Ulta;            
            brain.ulta_activate = false;
        }

        if (brain.target == null && !brain.die && curr_state != State.Ulta)
        {
            StopAction();
            curr_state = State.none;
        }

        if (curr_state == State.none)
        {
            ChangeState();
        } 
        else
        {
            #region Move To Target
            if (curr_state == State.MoveToTarget && !brain.stun)
            {
                //���� ��������
                if (Vector3.Distance(transform.position, brain.target.position) <= min_dist_attack)
                {
                    brain.StopMovement();

                    if (attack_access)
                    {
                        curr_state = State.Attack;
                        attack_start = true;
                    }
                } 
                else
                {
                    //����� � �����
                    brain.Movement(brain.target);
                }
            }
            #endregion

            #region Move To Selection
            if (curr_state == State.MoveToSelection)
            {
                //���� ��������
                if (Vector3.Distance(transform.position, brain.target.position) <= 0.5f)
                {
                    brain.StopMovement();
                    brain.selection_active = false;
                    brain.target = null;
                    curr_state = State.none;                    
                }
                else
                {
                    //����� � �����
                    brain.Movement(brain.target);
                }
            }
            #endregion
        }

        if (brain.health <= 0 && !brain.die)
        {
            brain.die = true;
            curr_state = State.Die;
            Die();
        }
    }

    void ChangeState()
    {
        if (brain.target != null)
        {
            if (Vector3.Distance(transform.position, brain.target.position) > min_dist_attack)
            {
                curr_state = State.MoveToTarget;
            }

            if (Vector3.Distance(transform.position, brain.target.position) <= min_dist_attack)
            {
                if (attack_access)
                {
                    brain.StopMovement();

                    curr_state = State.Attack;
                    attack_start = true;
                } 
                else
                {
                    brain.StopMovement();
                }
            }
        }
    }

    public void StopAction()
    {
        StopAllCoroutines();
        brain.StopMovement();
    }    

    void Die()
    {
        if (brain.team == Brain.Team.team_1)
            GameObject.Find("GameController").GetComponent<BattleController>().team_2_kills++;
        else
            GameObject.Find("GameController").GetComponent<BattleController>().team_1_kills++;

        brain.StopMovement();
        brain.anim.SetTrigger("die");

        gameObject.GetComponent<CharacterController>().enabled = false;
        gameObject.GetComponent<Pathfinding.DynamicGridObstacle>().enabled = false;

        StartCoroutine(Respawn());
    }

    IEnumerator Respawn()
    {        
        yield return new WaitForSeconds(3);
        brain.stun = false;

        brain.health = brain.start_health;
        gameObject.transform.position = brain.start_pos;

        gameObject.GetComponent<CharacterController>().enabled = true;
        gameObject.GetComponent<Pathfinding.DynamicGridObstacle>().enabled = true;

        brain.anim.SetTrigger("respawn");
        brain.die = false;
        
        curr_state = State.none;
    }
}
