using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassMage : MonoBehaviour
{
    public enum State
    {
        none,
        RoundStart,
        MoveToTarget,
        Die,
        Attack,
        Ulta,
        MoveToSelection,
        MoveBack
    }
    public State curr_state;

    Brain brain;

    public float rotation_speed;

    public float min_dist_attack;  //����������� ��������� ��� �����
    public float attack_cd;
    public float attack_time;

    [HideInInspector]
    public bool attack_start;
    //[HideInInspector]
    public bool attack_access;
    //[HideInInspector]
    public bool target_visible = false;

    public LayerMask ray_layer;
    public Transform ray_pos;

    public GameObject hit_gm;


    private void Start()
    {
        brain = GetComponent<Brain>();

        attack_access = true;
    }

    private void Update()
    {
        //if (brain.ulta_activate && !brain.stun)
        //{
        //    curr_state = State.Ulta;
        //    StopAction();
        //}

        if (brain.target == null && !brain.die)
        {
            StopAction();
            curr_state = State.none;
        }

        if (curr_state == State.none)
        {
            ChangeState();
        }
        else
        {
            if (curr_state == State.MoveBack && !brain.stun)
            {
                brain.MoveBack();
            }

            #region ����� �� ���������?
            if (brain.target != null)
            {
                if (attack_access && Vector3.Distance(transform.position, brain.target.position) <= min_dist_attack && !brain.stun)
                {
                    StopAction();
                    curr_state = State.Attack;
                    attack_start = true;
                }
            }
            #endregion

            #region Move To Target
            if (curr_state == State.MoveToTarget && brain.target != null && !brain.stun)
            {
                //���� ��������
                if (Vector3.Distance(transform.position, brain.target.position) <= min_dist_attack)
                {
                    brain.StopMovement();
                }
                else
                {
                    //����� � �����
                    brain.Movement(brain.target);
                }
            }
            #endregion

            #region Move To Selection
            if (curr_state == State.MoveToSelection)
            {
                //���� ��������
                if (Vector3.Distance(transform.position, brain.target.position) <= 1)
                {
                    brain.StopMovement();
                    brain.target = null;
                    curr_state = State.none;
                    brain.selection_active = false;
                    attack_access = true;
                }
                else
                {
                    //����� � �����
                    brain.Movement(brain.target);
                }
            }
            #endregion
        }

        if (brain.health <= 0 && !brain.die)
        {
            brain.die = true;
            curr_state = State.Die;
            Die();
        }

        if (curr_state != State.MoveBack)
        {
            brain.ai_path.enableRotation = false;
        }
        else
        {
            brain.ai_path.enableRotation = true;
        }

        #region ��������� ����������� �� ����
        if (brain.target != null)
        {
            CheckTargetDist();
        }
        #endregion
    }

    void ChangeState()
    {
        if (brain.target != null)
        {
            if (Vector3.Distance(transform.position, brain.target.position) > min_dist_attack)
            {
                curr_state = State.MoveToTarget;
            }

            if (Vector3.Distance(transform.position, brain.target.position) <= min_dist_attack)
            {
                if (attack_access)
                {
                    brain.StopMovement();

                    curr_state = State.Attack;
                    attack_start = true;
                }
                else
                {
                    brain.StopMovement();
                }
            }
        }
    }

    public void StopAction()
    {
        //StopAllCoroutines();
        brain.StopMovement();
    }

    void Die()
    {
        if (brain.team == Brain.Team.team_1)
            GameObject.Find("GameController").GetComponent<BattleController>().team_2_kills++;
        else
            GameObject.Find("GameController").GetComponent<BattleController>().team_1_kills++;

        brain.StopMovement();
        brain.anim.SetTrigger("die");
        gameObject.GetComponent<CharacterController>().enabled = false;
        gameObject.GetComponent<Pathfinding.DynamicGridObstacle>().enabled = false;

        StartCoroutine(Respawn());
    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(3);
        brain.stun = false;

        attack_start = false;
        brain.anim.SetTrigger("respawn");

        brain.health = brain.start_health;
        gameObject.transform.position = brain.start_pos;

        gameObject.GetComponent<CharacterController>().enabled = true;
        gameObject.GetComponent<Pathfinding.DynamicGridObstacle>().enabled = true;

        brain.die = false;
        attack_access = true;

        curr_state = State.none;
    }

    //��������� ������������ �� ����
    public void CheckTargetDist()
    {
        if (curr_state != State.MoveBack && !brain.die)
        {
            //LookAt
            Vector3 lookPos = brain.target.transform.position - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotation_speed);


            RaycastHit hit;

            Physics.Raycast(ray_pos.position, transform.forward, out hit, Mathf.Infinity, ray_layer);
            Debug.DrawRay(ray_pos.position, transform.forward * hit.distance, Color.green);

            if (hit.collider != null)
            {
                hit_gm = hit.collider.gameObject;
                if (hit.collider.gameObject.transform == brain.target)
                {
                    target_visible = true;
                }
                else
                {
                    target_visible = false;
                }
            }
            else
            {
                target_visible = false;
            }
        }
    }
}
