using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HillerVFX : MonoBehaviour
{
    public Transform target;
    public int team;

    public List<ParticleSystem> particles;

    private void Start()
    {
        if (team == 1)
        {
            foreach (ParticleSystem part in particles)
            {
                part.startColor = new Color(0, 0.4904165f, 1, 1);
            }
        }

        if (team == 2)
        {
            foreach (ParticleSystem part in particles)
            {
                part.startColor = new Color(1, 0.1192866f, 0, 1);
            }
        }
    }

    private void Update()
    {
        transform.position = target.position;
    }
}
