using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BananBomb : MonoBehaviour
{
    public GameObject father;
    public ParticleSystem part_explosion;
    public ParticleSystem part_spawn;
    public int damage;

    float min_x = -11;
    float max_x = 11;
    float min_z = -2;
    float max_z = 12;

    bool inst = true;

    IEnumerator StopSpawn()
    {
        yield return new WaitForSeconds(0.5f);
        inst = false;
    }


    private void Start()
    {
        Spawn();
        StartCoroutine(StopSpawn());
        part_spawn.Play();
    }

    void Spawn()
    {
        transform.position = new Vector3(Random.Range(min_x, max_x), 1, Random.Range(min_z, max_z));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "enemy")
        {
            if (other.gameObject.GetComponent<Brain>().team != father.GetComponent<Brain>().team)
            {
                Explosion(other.gameObject);
                Destroy(gameObject);
            }
        } else if (inst)
        {
            Spawn();
        }
    }

    void Explosion(GameObject enemy)
    {
        ParticleSystem part = Instantiate(part_explosion, transform.position, transform.rotation);
        Destroy(part, 5);
        enemy.GetComponent<Brain>().Hit(father, father.GetComponent<Brain>().damage * 3);
    }
}
