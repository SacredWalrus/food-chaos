using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharBananAttack : MonoBehaviour
{
    Brain brain;
    ClassRange class_range;
    public LineRenderer shot_line_1, shot_line_2;

    public GameObject bomb;

    public Transform shot_pos_1, shot_pos_2;

    private void Start()
    {
        brain = GetComponent<Brain>();
        class_range = GetComponent<ClassRange>();
    }

    private void Update()
    {
        if (class_range.curr_state == ClassRange.State.Attack && class_range.attack_access && !brain.die && brain.target != null && !brain.stun)
        {
            class_range.attack_access = false;
            StartCoroutine(Attack());
        }

        if (brain.die || brain.stun)
        {
            shot_line_1.enabled = false;
            shot_line_2.enabled = false;
            class_range.curr_clip = class_range.max_clip;
            class_range.attack_access = true;
        }

        if (brain.ulta_activate && !brain.stun)
        {
            Ulta();
            brain.ulta_activate = false;
        }
    }

    public void StopAttack()
    {
        StopAllCoroutines();
        shot_line_1.enabled = false;
        shot_line_2.enabled = false;
        class_range.attack_access = true;
        class_range.attack_access = false;
        class_range.curr_clip = class_range.max_clip;
    }

    IEnumerator Attack()
    {
        //LookAt
        Vector3 lookPos = brain.target.transform.position - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 1);

        brain.anim.SetTrigger("attack");

        shot_line_1.enabled = true;
        shot_line_1.SetPosition(0, shot_pos_1.position);
        shot_line_1.SetPosition(1, new Vector3(brain.target.position.x, brain.target.position.y + 0.6f, brain.target.position.z));

        shot_line_2.enabled = true;
        shot_line_2.SetPosition(0, shot_pos_2.position);
        shot_line_2.SetPosition(1, new Vector3(brain.target.position.x, brain.target.position.y + 0.6f, brain.target.position.z));

        yield return new WaitForSeconds(class_range.attack_time - 0.1f);

        shot_line_1.enabled = false;
        shot_line_2.enabled = false;

        yield return new WaitForSeconds(0.1f);

        class_range.curr_clip--;
        if (brain.target != null)
            brain.target.GetComponent<Brain>().Hit(gameObject, brain.damage);

        if (class_range.curr_clip <= 0)
        {
            class_range.attack_access = false;
            class_range.curr_state = ClassRange.State.MoveBack;            
            StartCoroutine(Reloading());
        }
        else
        {
            class_range.attack_access = true;
        }
    }

    public IEnumerator Reloading()
    {
        yield return new WaitForSeconds(class_range.reloading_time);

        brain.FindTarget();
        class_range.curr_clip = class_range.max_clip;
        class_range.attack_access = true;
        class_range.curr_state = ClassRange.State.none;
    }

    public void Ulta()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject gm = Instantiate(bomb, transform.position, transform.rotation);
            gm.transform.eulerAngles = new Vector3(0, Random.Range(0f, 360f), 90);
            gm.GetComponent<BananBomb>().father = gameObject;
        }
    }
}
