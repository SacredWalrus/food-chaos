using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharSodaBullet : MonoBehaviour
{
    public Brain brain;
    public GameObject father;
    public Transform target;
    public GameObject mesh;
    public float speed;

    float speed_x, speed_y, speed_z;

    private void Start()
    {
        target = brain.target;

        //transform.LookAt(target);

        mesh.transform.eulerAngles = new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));

        speed_x = Random.Range(5, 25);
        speed_y = Random.Range(5, 25);
        speed_z = Random.Range(5, 25);

        Destroy(gameObject, 5);
    }

    private void Update()
    {
        mesh.transform.Rotate(new Vector3(speed_x, speed_y, speed_z));

        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "obstacles")
        {
            Destroy(gameObject);
        }

        if (other.tag == "enemy")
        {
            if (other.gameObject.GetComponent<Brain>().team != brain.team)
            {
                other.gameObject.GetComponent<Brain>().Hit(father, brain.damage);
                Destroy(gameObject);
            }
        }
    }
}
