using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharSodaUlta : MonoBehaviour
{
    public GameObject shield_obj;
    public float time_ulta;

    Brain brain;

    List<GameObject> character_shield = new List<GameObject>();

    private void Start()
    {
        brain = GetComponentInParent<Brain>();
    }

    private void Update()
    {
        if (brain.ulta_activate && !brain.die && !brain.stun)
        {
            StartCoroutine(Ulta());
            brain.ulta_activate = false;
        }
    }

    IEnumerator Ulta()
    {
        character_shield.Clear();

        foreach (GameObject gm in GameObject.Find("GameController").GetComponent<BattleController>().active_character)
        {
            if (gm.GetComponent<Brain>().team == brain.team && !gm.GetComponent<Brain>().die)
            {
                character_shield.Add(gm);
                gm.GetComponent<Brain>().shield = true;

                GameObject fx = Instantiate(shield_obj, gm.transform.position, transform.rotation);
                fx.GetComponent<Shield>().target = gm.transform;
                Destroy(fx, 5);
            }
        }

        yield return new WaitForSeconds(time_ulta);

        foreach (GameObject gm in character_shield)
        {
            gm.GetComponent<Brain>().shield = false;
        }
    }
}
