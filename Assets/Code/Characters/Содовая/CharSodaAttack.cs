using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharSodaAttack : MonoBehaviour
{
    Brain brain;
    ClassHiller class_hiller;

    public GameObject bullet;


    private void Start()
    {
        brain = GetComponent<Brain>();
        class_hiller = GetComponent<ClassHiller>();
    }

    private void Update()
    {
        if (class_hiller.attack_start && !brain.die && brain.target != null && !brain.stun)
        {
            if (class_hiller.attack_access)
            {
                StartCoroutine(Attack());
                class_hiller.attack_start = false;
                class_hiller.attack_access = false;
            }
        }

        if (brain.die || brain.target == null || brain.stun)
        {
            StopAllCoroutines();
            class_hiller.attack_access = true;
        }
    }

    IEnumerator Attack()
    {
        brain.anim.SetTrigger("attack");

        Debug.Log("Attack");

        //LookAt
        Vector3 lookPos = brain.target.transform.position - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, class_hiller.rotation_speed);

        GameObject bullet_1 = Instantiate(bullet, new Vector3(transform.position.x, 1.58f, transform.position.z), transform.rotation);
        bullet_1.GetComponent<CharSodaBullet>().father = gameObject;
        bullet_1.GetComponent<CharSodaBullet>().brain = brain;

        yield return new WaitForSeconds(0.2f);

        //LookAt
        lookPos = brain.target.transform.position - transform.position;
        lookPos.y = 0;
        rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, class_hiller.rotation_speed);

        GameObject bullet_2 = Instantiate(bullet, new Vector3(transform.position.x, 1.58f, transform.position.z), transform.rotation);
        bullet_2.GetComponent<CharSodaBullet>().father = gameObject;
        bullet_2.GetComponent<CharSodaBullet>().brain = brain;

        yield return new WaitForSeconds(0.2f);

        //LookAt
        lookPos = brain.target.transform.position - transform.position;
        lookPos.y = 0;
        rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, class_hiller.rotation_speed);

        GameObject bullet_3 = Instantiate(bullet, new Vector3(transform.position.x, 1.58f, transform.position.z), transform.rotation);
        bullet_3.GetComponent<CharSodaBullet>().father = gameObject;
        bullet_3.GetComponent<CharSodaBullet>().brain = brain;

        yield return new WaitForSeconds(0.2f);

        StartCoroutine(AttackCoolDown());        
    }

    IEnumerator AttackCoolDown()
    {
        class_hiller.curr_state = ClassHiller.State.MoveBack;
        yield return new WaitForSeconds(class_hiller.attack_cd);
        class_hiller.curr_state = ClassHiller.State.none;
        class_hiller.attack_access = true;
    }
}
