using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    [Header("������� �����")]
    public List<float> hp_char_1;
    public List<float> dmg_char_1;
    public List<float> speed_char_1;

    [Header("�����")]
    public List<float> hp_char_2;
    public List<float> dmg_char_2;
    public List<float> speed_char_2;

    [Header("����")]
    public List<float> hp_char_3;
    public List<float> dmg_char_3;
    public List<float> speed_char_3;

    [Header("�������")]
    public List<float> hp_char_4;
    public List<float> dmg_char_4;
    public List<float> speed_char_4;
}
