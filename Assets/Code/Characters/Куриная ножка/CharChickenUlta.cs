using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharChickenUlta : MonoBehaviour
{
    public float rotate_speed;
    public float timer_ulta;
    private float timer;

    public GameObject trail;

    public GameObject father;
    ClassTank class_tank;
    Brain brain;

    private void Start()
    {
        brain = GetComponentInParent<Brain>();
        class_tank = GetComponentInParent<ClassTank>();

        timer = timer_ulta;

        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<BoxCollider>().enabled = false;

        trail.SetActive(false);
    }

    private void Update()
    {
        if (class_tank.curr_state == ClassTank.State.Ulta && !brain.die && !brain.stun)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            gameObject.GetComponent<BoxCollider>().enabled = true;

            trail.SetActive(true);
            brain.Movement(brain.target);

            brain.ai_path.enableRotation = false;

            father.transform.Rotate(new Vector3(0, rotate_speed, 0));

            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                gameObject.GetComponent<MeshRenderer>().enabled = false;
                gameObject.GetComponent<BoxCollider>().enabled = false;
                trail.SetActive(false);
                brain.ai_path.enableRotation = true;
                class_tank.curr_state = ClassTank.State.none;
                timer = timer_ulta;
            }    
        }

        if (class_tank.curr_state == ClassTank.State.Die || brain.stun)
        {
            UltOff();
        }
    }

    void UltOff()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<BoxCollider>().enabled = false;
        trail.SetActive(false);
        brain.ai_path.enableRotation = true;
        timer = timer_ulta;
    }
}
