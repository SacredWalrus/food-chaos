using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharChickenAttack : MonoBehaviour
{
    Brain brain;
    ClassTank class_tank;

    public GameObject attack_collider;


    private void Start()
    {
        brain = GetComponent<Brain>();
        class_tank = GetComponent<ClassTank>();
    }

    private void Update()
    {
        if (class_tank.attack_start && !brain.die && brain.target != null && !brain.stun && class_tank.curr_state != ClassTank.State.Ulta)
        {
            StartCoroutine(Attack());
            class_tank.attack_start = false;
            class_tank.attack_access = false;
        }

        if (brain.die || class_tank.curr_state == ClassTank.State.Ulta || brain.stun)
        {
            StopAllCoroutines();
            attack_collider.SetActive(false);
            class_tank.attack_access = true;
        }
    }

    IEnumerator Attack()
    {
        brain.anim.SetTrigger("attack");

        //LookAt
        Vector3 lookPos = brain.target.transform.position - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, class_tank.rotation_speed);

        yield return new WaitForSeconds(0.1f);

        attack_collider.SetActive(true);

        yield return new WaitForSeconds(class_tank.attack_time - 0.2f);

        attack_collider.SetActive(false);

        yield return new WaitForSeconds(0.1f);

        StartCoroutine(AttackCoolDown());

        if (class_tank.curr_state != ClassTank.State.Ulta)
            class_tank.curr_state = ClassTank.State.none;
    }

    IEnumerator AttackCoolDown()
    {
        yield return new WaitForSeconds(class_tank.attack_cd);
        class_tank.attack_access = true;
    }
}
