using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharChipsUlta : MonoBehaviour
{
    public GameObject stun_obj;
    public float time_ulta;

    Brain brain;

    List<GameObject> character_stun = new List<GameObject>();

    private void Start()
    {
        brain = GetComponentInParent<Brain>();
    }

    private void Update()
    {
        if (brain.ulta_activate && !brain.die && !brain.stun)
        {
            StartCoroutine(Ulta());
            brain.ulta_activate = false;
        }
    }

    IEnumerator Ulta()
    {
        character_stun.Clear();

        foreach (GameObject gm in GameObject.Find("GameController").GetComponent<BattleController>().active_character)
        {
            if (gm.GetComponent<Brain>().team != brain.team && !gm.GetComponent<Brain>().die)
            {
                character_stun.Add(gm);
                gm.GetComponent<Brain>().stun = true;
                gm.GetComponent<Brain>().StopMovement();
                gm.GetComponent<Brain>().Hit(gameObject, brain.damage);

                GameObject fx = Instantiate(stun_obj, gm.transform.position, transform.rotation);
                Destroy(fx, 5);
            }
        }

        yield return new WaitForSeconds(time_ulta);

        foreach (GameObject gm in character_stun)
        {
            gm.GetComponent<Brain>().stun = false;
        }
    }
}
