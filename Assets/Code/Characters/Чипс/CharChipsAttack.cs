using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharChipsAttack : MonoBehaviour
{
    Brain brain;
    ClassMage class_mage;

    public GameObject fx_attack;


    private void Start()
    {
        brain = GetComponent<Brain>();
        class_mage = GetComponent<ClassMage>();
    }

    private void Update()
    {
        if (class_mage.attack_start && !brain.die && brain.target != null && !brain.stun)
        {
            StartCoroutine(Attack());
            class_mage.attack_start = false;
            class_mage.attack_access = false;
        }

        if (brain.die || brain.stun)
        {
            StopAllCoroutines();
            class_mage.attack_access = true;
        }
    }

    IEnumerator Attack()
    {
        brain.anim.SetTrigger("attack");        

        //LookAt
        Vector3 lookPos = brain.target.transform.position - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, class_mage.rotation_speed);

        GameObject fx = Instantiate(fx_attack, transform.position, transform.rotation);
        Destroy(fx, 4);
        fx.GetComponent<CharChipsCollider>().father = gameObject;
        fx.GetComponent<CharChipsCollider>().brain = brain;

        yield return new WaitForSeconds(class_mage.attack_time);

        StartCoroutine(AttackCoolDown());
        class_mage.attack_access = false;
        class_mage.curr_state = ClassMage.State.MoveBack;
    }

    IEnumerator AttackCoolDown()
    {
        yield return new WaitForSeconds(class_mage.attack_cd);
        class_mage.attack_access = true;
        brain.FindTarget();
        class_mage.curr_state = ClassMage.State.none;
    }
}
