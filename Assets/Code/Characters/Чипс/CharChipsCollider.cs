using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharChipsCollider : MonoBehaviour
{
    public Brain brain;
    public GameObject father;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy" && other.gameObject.GetComponent<Brain>().team != brain.team)
        {
            other.gameObject.GetComponent<Brain>().Hit(father, brain.damage);
        }
    }
}
