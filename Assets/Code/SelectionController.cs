using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionController : MonoBehaviour
{
    public enum Pet
    {
        Chicken,
        Banan
    }
    public Pet curr_pet;

    public GameObject father;
    public Brain brain;

    MousePosition mouse;
    bool tap = false;

    public LineRenderer line;
    public GameObject selection_circle;

    private void Start()
    {
        tap = false;

        line.enabled = false;
        //selection_circle.SetActive(false);
    }

    private void OnMouseDown()
    {
        if (!brain.die)
        {
            tap = true;
            mouse.visual_accept = true;
            line.enabled = true;
            mouse.ignore_object = father;

            //selection_circle.SetActive(true);
        }        
    }

    private void OnMouseUp()
    {
        if (!brain.die)
        {
            tap = false;

            mouse.visual_accept = false;

            line.SetPosition(0, new Vector3(0, 0, 0));
            line.SetPosition(1, new Vector3(0, 0, 0));
            line.enabled = false;
            //selection_circle.SetActive(false);

            if (mouse.target != null && mouse.target != gameObject)
            {
                brain.target = mouse.target.GetComponentInChildren<SelectionController>().father.transform;
                brain.selection_active = true;

                switch (curr_pet)
                {
                    case Pet.Chicken:
                        father.GetComponent<ClassTank>().curr_state = ClassTank.State.none;
                        break;
                    case Pet.Banan:
                        father.GetComponent<CharBananAttack>().StopAttack();
                        father.GetComponent<ClassRange>().attack_access = true;
                        father.GetComponent<ClassRange>().curr_state = ClassRange.State.none;
                        break;
                }

                Debug.Log("ChangeTarget");
            }
            else
            {
                GameObject empty;
                GameObject new_target;

                switch (curr_pet)
                {
                    case Pet.Chicken:
                        father.GetComponent<ClassTank>().StopAction();

                        empty = new GameObject("new target");
                        new_target = Instantiate(empty, mouse.transform.position, transform.rotation);

                        father.GetComponent<Brain>().target = new_target.transform;
                        father.GetComponent<ClassTank>().curr_state = ClassTank.State.MoveToSelection;
                        break;
                    case Pet.Banan:
                        father.GetComponent<ClassRange>().StopAction();

                        empty = new GameObject("new target");
                        new_target = Instantiate(empty, mouse.transform.position, transform.rotation);

                        father.GetComponent<CharBananAttack>().StopAttack();
                        father.GetComponent<Brain>().target = new_target.transform;
                        
                        father.GetComponent<ClassRange>().curr_state = ClassRange.State.MoveToSelection;
                        break;
                }
            }
        }
    }

    private void Update()
    {
        mouse = GameObject.Find("mouse").GetComponent<MousePosition>();

        if (tap)
        {
            line.SetPosition(0, new Vector3(transform.position.x, 1.2f, transform.position.z));
            line.SetPosition(1, new Vector3(mouse.transform.position.x, 1.2f, mouse.transform.position.z));
        }

        if (brain.die && tap)
        {
            tap = false;

            mouse.visual_accept = false;

            line.SetPosition(0, new Vector3(0, 0, 0));
            line.SetPosition(1, new Vector3(0, 0, 0));
            line.enabled = false;
            //selection_circle.SetActive(false);
        }
    }
}
