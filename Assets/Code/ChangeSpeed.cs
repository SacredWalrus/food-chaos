using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangeSpeed : MonoBehaviour
{
    int curr_speed;
    public TMP_Text t_speed;
    public int max_speed;

    private void Start()
    {
        curr_speed = 0;
        But_ChangeSpeed();
        //Time.timeScale = 1;
    }

    public void But_ChangeSpeed()
    {
        if (curr_speed == max_speed) curr_speed = 1;
        else curr_speed += 1;

        t_speed.text = "x" + curr_speed;
        Time.timeScale = curr_speed;
    }
}
