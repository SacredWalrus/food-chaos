using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class MenuController : MonoBehaviour
{
    public int active_character_num = -10;
    public List<GameObject> img_outline_obj;
    public List<Image> img_character;
    public List<Sprite> spr_character;

    public List<int> character_num;


    public void Start()
    {
        for (int i = 0; i < character_num.Count; i++)
        {
            string prefs_speed = "character" + i + "speed";
            PlayerPrefs.SetInt(prefs_speed, 1);

            string prefs_damage = "character" + i + "damage";
            PlayerPrefs.SetInt(prefs_damage, 1);

            string prefs_health = "character" + i + "health";
            PlayerPrefs.SetInt(prefs_health, 1);
        }
    }

    #region Buttons
    public void But_PvP()
    {
        PlayerPrefs.SetString("play_mode", "pvp");
        Application.LoadLevel("Battle");
        //Application.LoadLevel("TestOrient");
    }

    public void But_PvE()
    {
        PlayerPrefs.SetString("play_mode", "pve");
        Application.LoadLevel("Battle");
        //Application.LoadLevel("TestOrient");
    }

    public void But_Character(int num)
    {
        foreach (GameObject gm in img_outline_obj)
        {
            gm.SetActive(false);
        }

        img_outline_obj[num].SetActive(true);

        active_character_num = num;
        UpdateStats(num);        
    }

    public void But_Character_Mass(int num)
    {
        if (active_character_num != -10)
        {
            img_character[active_character_num].sprite = spr_character[num];
            character_num[active_character_num] = num;

            string prefs = "character" + active_character_num + "num";
            PlayerPrefs.SetInt(prefs, num);
            Debug.Log(prefs + " " + num);
        }
    }

    public void RandomTeam(int team_num)
    {

    }    
    #endregion

    #region Upgrade
    [Header("Upgrade")]
    public TMP_Text t_speed;
    public TMP_Text t_damage;
    public TMP_Text t_health;

    public int curr_character_num;

    void UpdateStats(int num)
    {
        string prefs_speed = "character" + num + "speed";
        t_speed.text = PlayerPrefs.GetInt(prefs_speed) + "/10";

        string prefs_damage = "character" + num + "damage";
        t_damage.text = PlayerPrefs.GetInt(prefs_damage) + "/10";

        string prefs_health = "character" + num + "health";
        t_health.text = PlayerPrefs.GetInt(prefs_health) + "/10";
    }

    public void But_Upgrade_Plus(int id)
    {
        string prefs_speed = "character" + active_character_num + "speed";
        string prefs_damage = "character" + active_character_num + "damage";
        string prefs_health = "character" + active_character_num + "health";

        if (id == 1 && PlayerPrefs.GetInt(prefs_speed) < 10)
        {
            PlayerPrefs.SetInt(prefs_speed, PlayerPrefs.GetInt(prefs_speed) + 1);

            UpdateStats(active_character_num);
        }

        if (id == 2 && PlayerPrefs.GetInt(prefs_damage) < 10)
        {            
            PlayerPrefs.SetInt(prefs_damage, PlayerPrefs.GetInt(prefs_damage) + 1);

            UpdateStats(active_character_num);
        }

        if (id == 3 && PlayerPrefs.GetInt(prefs_health) < 10)
        {            
            PlayerPrefs.SetInt(prefs_health, PlayerPrefs.GetInt(prefs_health) + 1);

            UpdateStats(active_character_num);
        }
    }

    public void But_Upgrade_Minus(int id)
    {
        string prefs_speed = "character" + active_character_num + "speed";
        string prefs_damage = "character" + active_character_num + "damage";
        string prefs_health = "character" + active_character_num + "health";

        if (id == 1 && PlayerPrefs.GetInt(prefs_speed) > 1)
        {
            PlayerPrefs.SetInt(prefs_speed, PlayerPrefs.GetInt(prefs_speed) - 1);

            UpdateStats(active_character_num);
        }

        if (id == 2 && PlayerPrefs.GetInt(prefs_damage) > 1)
        {
            PlayerPrefs.SetInt(prefs_damage, PlayerPrefs.GetInt(prefs_damage) - 1);

            UpdateStats(active_character_num);
        }

        if (id == 3 && PlayerPrefs.GetInt(prefs_health) > 1)
        {
            PlayerPrefs.SetInt(prefs_health, PlayerPrefs.GetInt(prefs_health) - 1);

            UpdateStats(active_character_num);
        }
    }
    #endregion
}
